
/**
 * We get all the dependencies defined in the manifest
 * and collect it into a single object lookup.
 * This only collects "PRODUCTION" dependencies,
 * in other words, it does not grab the development dependencies.
 */

var debug = require('debug')('normalize-package:dependencies')
var parseGH = require('parse-github-repo-url')
var semver = require('semver')
var path = require('path')

var utils = require('./utils')

var removeJS = utils.removeJS
var sanitizeRelative = utils.sanitizeRelative

module.exports = Dependencies

function Dependencies(pkg) {
  if (!(this instanceof Dependencies)) return new Dependencies(pkg)

  this.root = pkg.root
  this.manifest = pkg.manifests
  pkg.dependencies = this.dependencies = Object.create(null)

  this.package()
  this.component()

  debug("%o", pkg.dependencies)
}

Dependencies.prototype.component = function () {
  var json = this.manifest.component
  if (!json) return
  var deps = json.dependencies
  if (!deps) return

  var dependencies = this.dependencies

  Object.keys(deps).forEach(function (name) {
    var version = deps[name]
    if (version === 'master') version = '*'
    // it's always github, so it should never fail
    name = name.toLowerCase()
    var repo = 'https://nlz.io/github/' + name + '/' + (version || '*')
    // allow looking up via `-`
    dependencies[name] =
    dependencies[removeJS(name)] =
    dependencies[name.replace('/', '-')] =
    dependencies[name.replace('/', '~')] = repo
    // looking up by name
    var name = name.split('/')[1]
    if (name) {
      // not sure why this would ever not work but whatever
      dependencies[name] =
      dependencies[removeJS(name)] = repo
    }
    // by component.json name
    dependencies[json.name] = repo
  })
}

Dependencies.prototype.package = function () {
  var json = this.manifest.package
  if (!json) return

  var dependencies = this.dependencies
  var deps = json.dependencies || {}

  Object.keys(deps).forEach(function (name) {
    var version = deps[name]
    var repo
    var gh

    if (semver.valid(version, true) || semver.validRange(version)) {
      // maybe make loose?
      repo = 'https://nlz.io/npm/-/' + name + '/' + version
    } else if (gh = parseGH(version)) {
      // github dependencies
      repo = 'https://nlz.io/github/' + gh[0] + '/' + gh[1] + '/' + (gh[2] || '*')
    } else {
      // everything else is *
      debug('resolving %s to *', name)
      repo = 'https://nlz.io/npm/-/' + name + '/*'
    }

    dependencies[name] = repo
  }, this)

  // lookup browser aliases, which we also attach to dependencies
  var browser = json.browser || {}
  if (typeof browser !== 'object') browser = {}
  var root = this.root

  Object.keys(browser).forEach(function (key) {
    var value = browser[key]

    // relative file
    if (key[0] === '.') {
      dependencies[path.resolve(root, sanitizeRelative(key))] = value[0] === '.'
        ? path.resolve(root, sanitizeRelative(value)) // relative file
        : dependencies[value] // module
      return
    }

    // module
    if (value === false) return dependencies[key] = false // i'm not sure what to do here
    if (value[0] === '.') return dependencies[key] = sanitizeRelative(value)
    dependencies[key] = dependencies[value] // alias
  })
}
