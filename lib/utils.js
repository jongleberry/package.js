
var fs = require('mz/fs')

// remove .js from names, specifically for component
exports.removeJS = function (str) {
  return str.replace(/\.js$/, '')
}

// do pretty .js stuff
exports.sanitizeRelative = function (target) {
  if (target[0] !== '.') target = './' + target
  if (/\/$/.test(target)) target += 'index.js'
  else if (!/\.js$/.test(target)) target += '.js'
  return target
}

// node-style relative deps
exports.isRelative = function (path) {
  return /^\.\.?\//.test(path)
}

// extensions to look up
var extensions = [
  '/index.js',
  '.js',
  '.json',
  '',
]

// find a file looking up extensions
exports.resolveLocalFilename = function* (filename) {
  for (var i = 0; i < extensions.length; i++) {
    var name = filename + extensions[i]
    if (yield fs.exists(name)) return name
  }
}

exports.stripRelative = function (string) {
  return string.replace(/^\.?\//, '')
}
