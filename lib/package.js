
/**
 * Collection all the manifest files into a single object.
 */

var fs = require('mz/fs')
var path = require('path')
var readdir = require('recursive-readdir')

module.exports = function* (pathname) {
  var manifests = yield {
    bower: readJSON(path.join(pathname, 'bower.json')),
    composer: readJSON(path.join(pathname, 'composer.json')),
    component: readJSON(path.join(pathname, 'component.json')),
    package: readJSON(path.join(pathname, 'package.json')),
  }
  return {
    root: pathname,
    manifests: manifests,
    files: yield readdir.bind(null, pathname)
  }
}

function readJSON(filename) {
  return fs.readFile(filename, 'utf8')
    .then(JSON.parse)
    .catch(ENOENT)
}

/* istanbul ignore next */
function ENOENT(err) {
  if (err.code !== 'ENOENT') throw err
}
