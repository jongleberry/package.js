
var profile = require('debug')('normalize-package:profile')

var Entrypoints = {
  css: require('./entrypoints/css'),
  js: require('./entrypoints/js'),
}

var Files = {
  js: require('./files/js'),
}

var Package = require('./package')
var Dependencies = require('./dependencies')

module.exports = function* (root) {
  profile('normalizing "%s"', root)
  var pkg = yield* Package(root)
  profile('got package "%s"', root)
  Dependencies(pkg)
  profile('got dependencies "%s"', root)
  yield Files.js(pkg)
  profile('normalized files "%s"', root)
  yield [
    Entrypoints.css(pkg),
    Entrypoints.js(pkg),
  ]
  profile('normalized entrypoints "%s"', root)
}

module.exports.rewrite = require('rewrite-js-dependencies/master')
