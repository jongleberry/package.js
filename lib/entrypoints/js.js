
var debug = require('debug')('normalize-package:entrypoints:js')
var parse = require('deps-parse').js
var path = require('path')

var utils = require('../utils')

var Entrypoint = module.exports = require('./entrypoint').extend()

Entrypoint.prototype.format = 'js'
Entrypoint.prototype.manifests = [
  'component',
  'package',
]

Entrypoint.prototype.component = function* () {
  var json = this.manifest.component
  var scripts = json.scripts || []
  if (!scripts.length) return debug('no component scripts found')

  scripts = scripts.map(utils.stripRelative)
  var main = utils.stripRelative(json.main || scripts[0])
  if (main === 'index.js') return debug('index.js already found')

  if (!/\.js$/.test(main)) return debug('non-js main found, aborting')
  if (~scripts.indexOf('index.js')) return debug('non-main index.js found, aborting')

  yield* this.proxy(main)
}

Entrypoint.prototype.package = function* () {
  var json = this.manifest.package
  var main = 'index.js'
  if (typeof json.browser === 'string') main = json.browser
  else if (json.main) main = json.main
  main = utils.stripRelative(main)

  var filename = path.join(this.root, main)
  var filename = yield* utils.resolveLocalFilename(filename)
  if (!filename) return debug('could not resolve %s', main)
  main = path.relative(this.root, filename)

  if (main === 'index.js') return debug('index.js is already main')

  yield* this.proxy(main)
}

Entrypoint.prototype.proxy = function* (filename) {
  filename = utils.stripRelative(filename)
  var mod
  try {
    var string = yield this.read(filename)
    mod = yield parse(string)
  } catch (err) {
    debug('error parsing %s', path.join(this.root, filename))
    return
  }

  // in the future, this should be inverted so that it defaults to `module`
  yield this.write(mod.type === 'module'
    ? 'export * from "./' + filename + '";'
    : 'module.exports = require("./' + filename + '");')
}
