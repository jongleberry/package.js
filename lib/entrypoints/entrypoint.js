
var inherits = require('util').inherits
var path = require('path')
var fs = require('mz/fs')

module.exports = Entrypoint

Entrypoint.extend = function () {
  var Super = this
  inherits(Entrypoint, Super)
  return Entrypoint
  function Entrypoint(pkg) {
    if (!(this instanceof Entrypoint)) return new Entrypoint(pkg)
    return Super.call(this, pkg)
  }
}

function Entrypoint(pkg) {
  this.root = pkg.root
  this.manifest = pkg.manifests
  return this.end()
}

Entrypoint.prototype.end = function* () {
  var manifest = this.manifest
  var names = this.manifests
  for (var i = 0; i < names.length; i++) {
    var name = names[i]
    if (this[name] && manifest[name])
      return yield* this[name]()
  }
}

Entrypoint.prototype.read = function (name) {
  name = name || ('index.' + this.format)
  return fs.readFile(path.join(this.root, name), 'utf8')
}

Entrypoint.prototype.write = function (val, name) {
  name = name || ('index.' + this.format)
  return fs.writeFile(path.join(this.root, name), val)
}
