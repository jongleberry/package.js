
var debug = require('debug')('normalize-package:entrypoints:css')

var utils = require('../utils')

var Entrypoint = module.exports = require('./entrypoint').extend()

Entrypoint.prototype.format = 'css'
Entrypoint.prototype.manifests = [
  'component',
  'package',
]

Entrypoint.prototype.component = function* () {
  var json = this.manifest.component
  var styles = json.styles || []
  if (!styles.length) return debug('component styles not found')

  if (styles.length === 1 && styles[0] === 'index.css') return debug('index.css found, no entry point required')

  styles = styles.map(utils.stripRelative)
  if (styles.length > 1 && ~styles.indexOf('index.css')) return debug('index.css found, aborting')

  yield this.write('\n' + styles.map(toImport).join('\n') + '\n')
}

Entrypoint.prototype.package = function* () {
  var json = this.manifest.package
  var style = json.style
  if (!style) return debug('package style not found')

  if (!/\.\//.test(style)) style = './' + style
  if (!/\.css/.test(style)) style += '.css'
  if (style === './index.css') return debug('index.css found')

  yield this.write('\n' + toImport(utils.stripRelative(style)) + '\n')
}

function toImport(uri) {
  return '@import "' + uri + '";'
}
