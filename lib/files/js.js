
var debug = require('debug')('normalize-package:files:js')
var rewrite = require('rewrite-js-dependencies/master')
var parse = require('deps-parse').js
var is = require('path-is')
var path = require('path')
var fs = require('mz/fs')

var builtins = require('../builtins')
var utils = require('../utils')

var isRelative = utils.isRelative
var sanitizeRelative = utils.sanitizeRelative
var resolveLocalFilename = utils.resolveLocalFilename

module.exports = function (pkg) {
  return pkg.files.map(function (file) {
    if (!/\.js$/.test(file)) return
    return normalize(pkg.root, file, pkg.dependencies)
  }).filter(Boolean)
}

// wrap in a try/catch to make sure an error is never returned
function* normalize(root, filename, dependencies) {
  try {
    yield* _normalize(root, filename, dependencies)
  } catch (err) {
    console.error(filename)
    console.error(err.message)
    console.error(err.stack)
  }
}

function* _normalize(root, filename, dependencies) {
  var string = yield fs.readFile(filename, 'utf8')
  debug('normalizing %s', root)
  var mod = yield parse(string)
  if (!mod.dependencies.length) return debug('no dependencies: %s', filename)

  var dir = path.dirname(filename)
  var renames = {}
  yield mod.dependencies.map(function* (id) {
    debug('checking %s', id)
    // don't do anything to absolute dependencies
    if (is.absolute(id)) return debug('absolute')

    // browser false stuff for browserify
    if (dependencies[id] === false) return renames[id] = builtins.empty

    if (isRelative(id)) {
      // resolve package.json aliases
      var target = dependencies[path.resolve(dir, sanitizeRelative(id))]
      if (target) {
        // why would this happen?!
        if (!(yield fs.exists(target))) return debug('browser alias not found: %s', target)
        renames[id] = sanitizeRelative(path.relative(dir, target))
        return
      }

      // lookup relative dependencies
      var name = yield resolveLocalFilename(path.resolve(dir, id))
      if (!name) return debug('local dependency not found: %s', id)
      renames[id] = sanitizeRelative(path.relative(dir, name))
      return
    }

    // yay, remove deps
    var frags = id.split('/')
    var _id = frags[0]
    var tail = frags.slice(1).join('/')
    if (tail) {
      tail = '/' + tail
      if (!/\.js$/.test(tail)) tail += '.js'
    }

    var target = dependencies[_id] || dependencies[_id.toLowerCase()]
    if (target) {
      if (target[0] === '.') {
        target = path.resolve(root, target)
        if (!(yield fs.exists(target))) return debug('browser alias not found: %s', target)
        renames[id] = sanitizeRelative(path.relative(dir, target))
        return
      }

      renames[id] = target + (tail || '/index.js')
      return
    }

    if (builtins[_id]) return renames[id] = builtins[id]
  })

  debug('renames: %o', renames)

  if (!Object.keys(renames).length) return

  var out = yield rewrite(string, renames)
  if (string === out) return debug('transformed into equivalent code: %s', filename)
  yield fs.writeFile(filename, out)
}
