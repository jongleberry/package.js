
var co = require('co')
var fs = require('mz/fs')
var path = require('path')
var assert = require('assert')
var rimraf = require('rimraf')
var mkdirp = require('mkdirp')

rimraf.sync(path.resolve('cache'))

var normalize = require('..')

it('pkg-false', co.wrap(function* () {
  var name = 'pkg-false'
  clean(name)
  yield write(name, 'package.json', {
    browser: {
      meh: false
    }
  })
  yield write(name, 'index.js', 'require("meh")')
  yield normalize(fixture(name))
  var js = yield read(name, 'index.js')
  assert(!~js.indexOf("meh"))
}))

it('pkg-main-no-js', co.wrap(function* () {
  var name = 'pkg-main-no-js'
  clean(name)
  yield write(name, 'package.json', { main: 'immutable' })
  yield write(name, 'immutable.js', 'console.log("lol")')
  yield normalize(fixture(name))
  var js = yield read(name, 'index.js')
  assert.equal('module.exports = require("./immutable.js");', js)
}))

it('pkg-exts', co.wrap(function* () {
  var name = 'pkg-exts'
  clean(name)
  yield write(name, 'index.js',
    "require('./a');\n" +
    "require('./b');\n" +
    "require('./c/');\n" +
    "require('./d.js');\n"
  )
  yield mkdirp.bind(null, fixture(name, 'a'))
  yield mkdirp.bind(null, fixture(name, 'c'))
  yield write(name, 'a/index.js', '')
  yield write(name, 'b.json', '{}')
  yield write(name, 'c/index.js', '')
  yield write(name, 'd.js')
  yield normalize(fixture(name))
  var js = yield read(name, 'index.js')
  assert(~js.indexOf('"./a/index.js"'))
  assert(~js.indexOf('"./b.json.js"'))
  assert(~js.indexOf('"./c/index.js"'))
  assert(~js.indexOf('./d.js'))
}))

it('pkg-lookup-dep', co.wrap(function* () {
  var name = 'pkg-lookup-dep'
  clean(name)
  yield write(name, 'package.json', {
    dependencies: {
      "emitter": "*",
      "mercury": "1"
    }
  })
  yield write(name, 'index.js',
    "require('emitter');\n" +
    "require('mercury/blah.js');\n" +
    "require('url');\n"
  )
  yield normalize(fixture(name))
  var js = yield read(name, 'index.js')
  assert(~js.indexOf('https://nlz.io/npm/-/emitter/*/index.js'))
  assert(~js.indexOf('https://nlz.io/npm/-/mercury/1/blah.js'))
  assert(~js.indexOf('https://nlz.io/npm/-/url/*/index.js'))
}))

it('pkg-browser', co.wrap(function* () {
  var name = 'pkg-browser'
  clean(name)
  yield write(name, 'package.json', {
    dependencies: {
      a: '1',
      b: '2',
      c: '3',
    },
    browser: {
      './index.js': './something.js',
      a: 'b',
      c: './something.js',
      d: false
    }
  })
  yield write(name, 'index.js',
    "require('./index.js');\n" +
    "require('a');\n" +
    "require('c');"
  )
  yield write(name, 'something.js', '')
  yield normalize(fixture(name))
  var js = yield read(name, 'index.js')
  assert(~js.indexOf('./something.js'))
  assert(~js.indexOf('https://nlz.io/npm/-/b/2/index.js'))
  assert(!~js.indexOf("require('./index.js');"))
  assert(!~js.indexOf("require('c');"))
}))

it('pkg-github-deps', co.wrap(function* () {
  var name = 'pkg-github-deps'
  clean(name)
  yield write(name, 'package.json', {
    dependencies: {
      emitter: 'component/emitter',
      domify: 'component/domify#1'
    }
  })
  yield write(name, 'index.js',
    "require('emitter');\n" +
    "require('domify/blah.js');\n"
  )
  yield normalize(fixture(name))
  var js = yield read(name, 'index.js')
  assert(~js.indexOf('https://nlz.io/github/component/emitter/*/index.js'))
  assert(~js.indexOf('https://nlz.io/github/component/domify/1/blah.js'))
}))

it('pkg-dep-unknown', co.wrap(function* () {
  var name = 'pkg-dep-unknown'
  clean(name)
  yield write(name, 'package.json', {
    dependencies: {
      emitter: 'emitter.gz',
    }
  })
  yield write(name, 'index.js', "require('emitter');")
  yield normalize(fixture(name))
  var js = yield read(name, 'index.js')
  assert(~js.indexOf('https://nlz.io/npm/-/emitter/*/index.js'))
}))

it('pkg-style', co.wrap(function* () {
  var name = 'pkg-style'
  clean(name)
  yield write(name, 'package.json', {
    style: 'bootstrap.css'
  })
  yield write(name, 'bootstrap.css', '')
  yield normalize(fixture(name))
  var css = yield read(name, 'index.css')
  assert.equal('@import "bootstrap.css";', css.trim())
}))

it('pkg-style-no-ext', co.wrap(function* () {
  var name = 'pkg-style-no-ext'
  clean(name)
  yield write(name, 'package.json', {
    style: 'bootstrap'
  })
  yield write(name, 'bootstrap.css', '')
  yield normalize(fixture(name))
  var css = yield read(name, 'index.css')
  assert.equal('@import "bootstrap.css";', css.trim())
}))

it('c8-lookup-dep', co.wrap(function* () {
  var name = 'c8-lookup-dep'
  clean(name)
  yield write(name, 'component.json', {
    scripts: ['file.js'],
    dependencies: {
      'component/emitter': '*'
    }
  })
  yield write(name, 'file.js',
    "require('emitter');\n" +
    // "require('component/emitter');\n" +
    "require('component-emitter');\n" +
    "require('component-emitter/index.js');\n"
  )
  yield normalize(fixture(name))
  var js = yield read(name, 'file.js')
  assert(~js.indexOf('https://nlz.io/github/component/emitter/*/index.js'))
  assert(!~js.indexOf("'emitter'"))
  assert(!~js.indexOf("'component-emitter'"))
  var js = yield read(name, 'index.js')
  assert.equal('module.exports = require("./file.js");', js)
}))

it('c8-master', co.wrap(function* () {
  var name = 'c8-master'
  clean(name)
  yield write(name, 'component.json', {
    scripts: ['index.js.js'],
    dependencies: {
      'component/emitter': 'master'
    }
  })
  yield write(name, 'index.js', 'require("emitter")')
  yield normalize(fixture(name))
  var js = yield read(name, 'index.js')
  assert(~js.indexOf('https://nlz.io/github/component/emitter/*/index.js'))
}))

it('c8-css-1', co.wrap(function* () {
  var name = 'c8-css-1'
  clean(name)
  yield write(name, 'component.json', {
    styles: ['blah.css']
  })
  yield write(name, 'blah.css', '')
  yield normalize(fixture(name))
  var css = yield read(name, 'index.css')
  assert.equal('@import "blah.css";', css.trim())
}))

it('c8-css-2', co.wrap(function* () {
  var name = 'c8-css-2'
  clean(name)
  yield write(name, 'component.json', {
    styles: ['blah.css', 'blob.css']
  })
  yield write(name, 'blah.css', '')
  yield write(name, 'blob.css', '')
  yield normalize(fixture(name))
  var css = yield read(name, 'index.css')
  assert(~css.indexOf('@import "blah.css";'))
  assert(~css.indexOf('@import "blob.css";'))
}))

function clean(name) {
  var dir = fixture(name)
  rimraf.sync(dir)
  mkdirp.sync(dir)
}

function read(name, file) {
  return fs.readFile(fixture(name, file), 'utf8')
}

function write(name, file, val) {
  if (typeof val !== 'string') val = JSON.stringify(val)
  return fs.writeFile(fixture(name, file), val)
}

function fixture(name, file) {
  return path.join(__dirname, 'fixtures', name, file || '')
}
