
# Normalize Packages

[![NPM version][npm-image]][npm-url]
[![Build status][travis-image]][travis-url]
[![Test coverage][coveralls-image]][coveralls-url]
[![Dependency Status][david-image]][david-url]
[![License][license-image]][license-url]
[![Downloads][downloads-image]][downloads-url]
[![Gittip][gittip-image]][gittip-url]

Normalizes packages against manifests:

- `package.json`
- `component.json`

Features:

- Rewrites and/or creates "main" entries to `index.<ext>` files
- Rewrites all dependencies to `https://nlz.io/` URLs so they work as assets

It does not support:

- CSS dependencies of packages - it only normalizes the current package's CSS files

[npm-image]: https://img.shields.io/npm/v/normalize-package.svg?style=flat-square
[npm-url]: https://npmjs.org/package/normalize-package
[github-tag]: http://img.shields.io/github/tag/normalize/package.js.svg?style=flat-square
[github-url]: https://github.com/normalize/package.js/tags
[travis-image]: https://img.shields.io/travis/normalize/package.js.svg?style=flat-square
[travis-url]: https://travis-ci.org/normalize/package.js
[coveralls-image]: https://img.shields.io/coveralls/normalize/package.js.svg?style=flat-square
[coveralls-url]: https://coveralls.io/r/normalize/package.js?branch=master
[david-image]: http://img.shields.io/david/normalize/package.js.svg?style=flat-square
[david-url]: https://david-dm.org/normalize/package.js
[license-image]: http://img.shields.io/npm/l/normalize-package.svg?style=flat-square
[license-url]: LICENSE.md
[downloads-image]: http://img.shields.io/npm/dm/normalize-package.svg?style=flat-square
[downloads-url]: https://npmjs.org/package/normalize-package
[gittip-image]: https://img.shields.io/gittip/jonathanong.svg?style=flat-square
[gittip-url]: https://www.gittip.com/jonathanong/
